package com.mars.module.tool.controller;

import com.mars.common.result.R;
import com.mars.common.response.PageInfo;
import com.mars.common.request.log.LogApiQueryRequest;
import com.mars.common.response.log.LogApiResponse;
import com.mars.module.tool.service.impl.LogApiServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口日志控制器
 *
 * @author 源码字节-程序员Mars
 */
@Api(tags = "日志管理-接口日志")
@RestController
@RequestMapping("/log/api")
@AllArgsConstructor
public class SysLogController {

    private final LogApiServiceImpl logApiService;

    /**
     * 获取接口日志
     *
     * @param queryDto 请求参数
     * @return R
     */
    @PostMapping("/list")
    @ApiOperation(value = "获取列表")
    public R list(@Validated @RequestBody LogApiQueryRequest queryDto) {
        PageInfo<LogApiResponse> pageInfo = logApiService.list(queryDto);
        return R.success(pageInfo);
    }

    /**
     * 删除
     *
     * @param id id
     * @return R
     */
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        logApiService.delete(id);
        return R.success();
    }
}
