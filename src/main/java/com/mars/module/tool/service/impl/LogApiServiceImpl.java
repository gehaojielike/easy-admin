package com.mars.module.tool.service.impl;


import com.mars.common.response.PageInfo;
import com.mars.common.request.log.LogApiQueryRequest;
import com.mars.module.tool.mapper.SysLogMapper;
import com.mars.common.response.log.LogApiResponse;
import com.mars.module.tool.service.ILogApiService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


/**
 * 接口日志Service
 *
 * @author 源码字节-程序员Mars
 */
@Service
@AllArgsConstructor
public class LogApiServiceImpl implements ILogApiService {

    private final SysLogMapper sysLogMapper;


    @Override
    public PageInfo<LogApiResponse> list(LogApiQueryRequest queryDto) {
        return PageInfo.build(sysLogMapper.selectPageList(queryDto.page(), queryDto));
    }

    @Override
    public void delete(Long id) {
        sysLogMapper.deleteById(id);
    }
}
