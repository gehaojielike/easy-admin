package com.mars.module.tool.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.GenSysMenu;
import com.mars.module.tool.request.GenSysMenuRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author mars
 */
public interface GenSysMenuMapper extends BasePlusMapper<GenSysMenu> {
    /**
     * 查询系统菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    List<GenSysMenu> selectMenuList(GenSysMenu menu);

    /**
     * 删除菜单管理信息
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    int deleteMenuById(Long menuId);

    /**
     * 根据名称模糊查询
     *
     * @param functionName functionName
     * @return List<GenSysMenu>
     */
    List<GenSysMenu> selectMenuListByName(@Param("functionName") String functionName);

    /**
     * 根据用户ID查询角色key
     *
     * @param userId 用户ID
     * @return String
     */
    Long selectRoleId(Long userId);
}
