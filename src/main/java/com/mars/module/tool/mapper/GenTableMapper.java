package com.mars.module.tool.mapper;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.request.tool.TableQueryRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.response.tool.TableListResponse;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.GenTable;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 业务 数据层
 *
 * @author mars
 */
public interface GenTableMapper extends BasePlusMapper<GenTable> {
    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    List<GenTable> selectGenTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    List<GenTable> selectDbTableListByNames(@Param("tableNames") List<String> tableNames);


    /**
     * 查询表名称业务信息
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    GenTable selectGenTableByName(String tableName);

    /**
     * 分页查询
     *
     * @param request 请求参数
     * @return PageInfo<TableListResponse>
     */
    IPage<TableListResponse> tableList(@Param("page") Page<TableListResponse> page, @Param("request") TableQueryRequest request);


    /**
     * 获取数据库列表
     *
     * @param tableNames 表名称
     * @return List<TableListResponse>
     */
    List<TableListResponse> selectDbTableListByTableNames(@Param("tableNames") List<String> tableNames);
}
