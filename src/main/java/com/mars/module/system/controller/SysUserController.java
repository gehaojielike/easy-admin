package com.mars.module.system.controller;

import com.mars.common.result.R;
import com.mars.common.response.PageInfo;
import com.mars.common.request.sys.*;
import com.mars.common.response.sys.SysUserListResponse;
import com.mars.module.system.service.impl.SysUserServiceImpl;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 用户管理控制器
 *
 * @author 源码字节-程序员Mars
 */
@Api(tags = "系统管理-用户管理")
@RestController
@RequestMapping("/sys/user")
@AllArgsConstructor
public class SysUserController {

    private final SysUserServiceImpl sysUserServiceImpl;

    /**
     * 列表查询
     *
     * @param request request
     * @return R
     */
    @PostMapping("/list")
    @ApiOperation(value = "获取列表")
    public R pageList(@RequestBody SysUserQueryRequest request) {
        return R.success(sysUserServiceImpl.pageList(request));
    }

    /**
     * 获取详情
     *
     * @param id id
     * @return R
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取详情")
    public R get(@PathVariable("id") Long id) {
        return R.success(sysUserServiceImpl.get(id));
    }

    /**
     * 新增
     *
     * @param request request
     * @return R
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增")
    public R add(@Validated @RequestBody SysUserAddRequest request) {
        sysUserServiceImpl.add(request);
        return R.success();
    }

    /**
     * 修改
     *
     * @param request request
     * @return R
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改")
    public R update(@Validated @RequestBody SysUserUpdateRequest request) {
        sysUserServiceImpl.update(request);
        return R.success();
    }

    /**
     * 删除
     *
     * @param id id
     * @return R
     */
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        sysUserServiceImpl.delete(id);
        return R.success();
    }

    /**
     * 导入
     *
     * @param file file
     * @return R
     */
    @PostMapping("imports")
    @ApiOperation(value = "导入")
    public R imports(MultipartFile file) throws Exception {
        String fileName = file.getOriginalFilename();
        if (fileName == null || "".equals(fileName)) {
            return null;
        }
        String[] columns = new String[]{"userName", "realName", "sex", "birthDate", "phone"};
//        List<SysUserAddRequest> list = ExcelUtils.importExcel(file, SysUserAddRequest.class, columns);
//        for (SysUserAddRequest addDto : list) {
//            sysUserServiceImpl.add(addDto);
//        }
        return R.success();
    }

    /**
     * 导出
     *
     * @param queryDto queryDto
     * @param request  request
     * @param response response
     */
    @PostMapping(value = "export")
    @ApiOperation(value = "导出")
    public void export(@RequestBody SysUserQueryRequest queryDto, HttpServletRequest request, HttpServletResponse response) throws IOException {
        queryDto.setPageSize(-1);
        PageInfo<SysUserListResponse> pageList = sysUserServiceImpl.pageList(queryDto);
        List<SysUserListResponse> list = pageList.getList();
        String fileName = "用戶信息";
        ExcelUtils.exportExcel(list, fileName, fileName, SysUserListResponse.class, fileName, response);
    }
}
