package com.mars.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户token
 *
 * @author 源码字节-程序员Mars
 */
@Data
@ApiModel(value = "用户上下文信息")
public class UserContextInfo {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "上下文信息")
    private Long id;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;

    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private Long timestamp;

}
