package com.mars.common.response.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 菜单VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysMenuResponse {

    @ApiModelProperty(value = "菜单ID")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String menuName;

    @ApiModelProperty(value = "父ID")
    private Long parentId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "权限标识")
    private String perms;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单类型 1.目录 2 菜单 3 按钮")
    private Integer menuType;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "子权限")
    private List<SysMenuResponse> children;


}
