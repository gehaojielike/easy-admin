package com.mars.common.util;

import com.mars.framework.handler.TransHandler;
import com.mars.module.tool.request.FieldRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-14 18:27:28
 */
public class SqlGeneratorUtils {


    /**
     * 生成表结构
     *
     * @param fieldInfoList  字段信息
     * @param tableName      表名称
     * @param isGenBaseField 是否生成基础字段
     * @param name           中文名称
     * @return String
     */
    public static String generateDynamicSQL(List<FieldRequest> fieldInfoList, String tableName, boolean isGenBaseField, String name) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("CREATE TABLE ").append("`").append(tableName).append("`").append(" (\n");
        fieldInfoList.sort(Comparator.comparingInt(FieldRequest::getSort));
        sqlBuilder.append("`id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',\n");
        for (FieldRequest fieldInfo : fieldInfoList) {

            String fieldName = fieldInfo.getFieldName();
            String fieldType = fieldInfo.getFieldType();
            if (fieldName.equals("sort")) {
                break;
            }
            Integer fieldLength = fieldInfo.getFieldLength();
            String fieldComment = fieldInfo.getFieldComment();

            // 添加字段定义
            sqlBuilder.append("`").append(fieldName).append("`").append(" ").append(fieldType);
            if (fieldLength != null) {
                if (fieldType.equals("VARCHAR")) {
                    sqlBuilder.append("(").append(fieldLength).append(") CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL");
                } else {
                    sqlBuilder.append("(").append(fieldLength).append(")");
                }
            }
            // 添加字段注释
            if (fieldComment != null) {
                sqlBuilder.append(" COMMENT '").append(fieldComment).append("',\n");
            }
        }
        appendBaseField(isGenBaseField, sqlBuilder);
        sqlBuilder.append("PRIMARY KEY (`id`) USING BTREE \n)");
        // 移除最后一个逗号
        sqlBuilder.deleteCharAt(sqlBuilder.length() - 1);
        sqlBuilder.append(") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='" + name + "';");
        return sqlBuilder.toString();
    }

    /**
     * 基础字段
     *
     * @param isGenBaseField isGenBaseField
     * @param sqlBuilder     sqlBuilder
     */
    private static void appendBaseField(boolean isGenBaseField, StringBuilder sqlBuilder) {
        if (isGenBaseField) {
            sqlBuilder.append("`create_by_id` bigint DEFAULT NULL COMMENT '创建人账号',\n");
            sqlBuilder.append("`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',\n");
            sqlBuilder.append("`create_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '创建人名称',\n");
            sqlBuilder.append("`update_by_id` bigint DEFAULT NULL COMMENT '更新人账号',\n");
            sqlBuilder.append("`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',\n");
            sqlBuilder.append("`update_by_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '更新人名称',\n");
            sqlBuilder.append("`deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除',\n");
        }
    }

}
