package com.mars.common.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Set;

/**
 * Http工具类
 *
 * @author 源码字节-程序员Mars
 */
public class HttpUtils {

    private static final String UTF_8 = "UTF-8";


    /**
     * 发送get请求
     *
     * @param url     请求地址
     * @param headers headers
     * @return String
     */
    public static String sendGet(String url, Map<String, String> headers) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        // 设置请求方法为GET
        connection.setRequestMethod("GET");
        // 设置Header
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            connection.setRequestProperty(entry.getKey(), entry.getValue());
        }
        // 读取响应内容
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        // 返回响应内容
        return response.toString();
    }


    /**
     * get请求
     *
     * @param url 请求地址
     * @return String
     */
    public static String doGet(String url) {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.connect();
            inputStream = connection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, UTF_8));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
            closeResources(bufferedReader, inputStream, null);
        }
        return null;
    }

    public static String get(String url, Map<String, String> params) {
        String param = transformParam(params);
        return doGet(url + "?" + param);
    }

    public static String post(String url, Map<String, String> params) {
        String param = transformParam(params);
        HttpURLConnection connection = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("charsert", UTF_8);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            outputStream = connection.getOutputStream();
            outputStream.write(param.getBytes(UTF_8));
            connection.connect();
            inputStream = connection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, UTF_8));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
            closeResources(bufferedReader, inputStream, outputStream);
        }
        return null;
    }

    public static String post(String url, String json) {
        HttpURLConnection connection = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            outputStream = connection.getOutputStream();
            outputStream.write(json.getBytes(UTF_8));
            inputStream = connection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, UTF_8));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
            closeResources(bufferedReader, inputStream, outputStream);
        }
        return null;
    }

    private static String transformParam(Map<String, String> params) {
        String result = null;
        StringBuilder sb = new StringBuilder();
        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            sb.append(key).append("=").append(params.get(key)).append("&");
        }
        if (sb.length() > 1) {
            result = sb.substring(0, sb.length() - 1).toString();
        }
        return result;
    }

    private static void closeResources(BufferedReader bufferedReader, InputStream inputStream, OutputStream outputStream) {
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
