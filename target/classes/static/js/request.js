

/**
 * 构建axios请求
 */
// const BASE_URL = 'http://47.99.40.195:8901'
const BASE_URL = 'http://localhost:8901'

const requests = axios.create({
    baseURL: BASE_URL,
    timeout: 10000
});

/**
 * 请求拦截器
 */
requests.interceptors.request.use(config => {
    let token = JSON.parse(localStorage.getItem("mars-token"));
    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

/**
 * 响应拦截器
 */
requests.interceptors.response.use(res => {
    if (res.data.code === '403') {
        setTimeout(()=>{
            Vue.prototype.$notify({
                title: res.data.message,
                type: 'error',
                duration: 2000
            })
            parent.location.href = '/login';
        },1000)
    }else if (res.data.code==='500'){
        Vue.prototype.$notify({
            title: res.data.message,
            type: 'error',
            duration: 2000
        })
    }

    if (res.request.responseType === 'arraybuffer') {
        return res;
    }
    return res.data;
});





